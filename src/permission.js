import router from './router/index';
import NProgress from "nprogress"//进度条插件
import 'nprogress/nprogress.css'//进度条样式
import website from "./const/website"
import {getToken,getCookie}from "@/utils/util.js"
import{filterAsyncRoutes} from "@/router/asyncRouter.js"
NProgress.configure({showSpinner:false})//进度条配置
const whiteList=["/login"];//白名单
router.beforeEach((to,from,next)=>{
//进度条开始
NProgress.start();
//获取路由meta中title，并设置给页面标题
document.title=`${to.name}-${website.title}`
const hasToken=getToken('token');
console.log('全局路由守卫=======>>>>>>');
console.log('to:',to);
console.log('from:',from);
if(hasToken){
    if(window.$store.getters.LoginState===401){
        NProgress.done()
        next()
    }else{
        NProgress.done()
        if(to.path==="/login"){
            document.title=`${from.name}-${website.title}`
            next({path:"/wel/home"})
        }else{
            next()
        }
    }
   
}else{
    NProgress.done()
    
    if(to.path==="/login"){
        next()
       
    }else{
        next({path:"/login"})
    }
}
   
    
})

router.afterEach(() => {
    // 结束精度条
    NProgress.done()
})
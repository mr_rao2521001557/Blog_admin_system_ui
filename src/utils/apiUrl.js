
// var baseUrl="http://192.168.1.22:8888"
const baseUrl="http://localhost:8888"

export default {
    loginSubmit:"/login",//登录提交用户信息
    getVerificationCode:'/login/verificationCode',//获取登录验证码
    imgBaseUrl:baseUrl+'/images/',//静态壁纸根路径
    getBackgroundImg:"/common/backgroundImage",//获取壁纸列表
    getBackgroundMusic:baseUrl+'/backgroundMusic/',//动态背景音乐

}
let key = "cty@2020"
let crypto;
try {
    crypto = require('crypto');//密码 node模块
} catch (err) {
    console.log('不支持 crypto')
}
/**
 * 加密
 * @param {明文} data 
 */
exports.aesEncrypt=function (data) {
    const cipher = crypto.createCipher('aes192', key);
    var crypted = cipher.update(data, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}
/**
 * 解密
 * @param {密文} encrypted 
 */
exports.aesDecrypt=function (encrypted) {
    const decipher = crypto.createDecipher('aes192', key);
    var decrypted = decipher.update(encrypted, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}

/**
 * 获取cookie
 */
exports.getCookie=function(cookieName){
    var cookie=document.cookie;
    console.log('cookie',cookie)
}
/**
 * 
 * @param {cookieName} cookieName 
 * @param {cookieValue} cookieValue 
 * @param {有效期，毫秒} time 
 */
exports.setCookie=function(cookieName,cookieValue,time,callback){
   var str=`${cookieName}=${cookieValue}`;
   var nowDate=new Date();
   var expectDate=new Date(nowDate.getTime()+time);
   if(time){
    str+=`;expires=${expectDate.toUTCString()}`
   }
   if(callback){
    callback(str);
   }
//    console.log('str',str)
   document.cookie=str
}

/**
 * 是否登录
 */
exports.isLogin=function(){
    var flag=false
       // 先获取所有的cookie
       var str = document.cookie;
       // 拆分成数组
       var arr = str.split("; ");
       // 循环判断
       for( var i=0,len=arr.length; i<len; i++ ){
           var tmp = arr[i];
           var ind = tmp.indexOf("=");
           var key = tmp.substring(0, ind);
           var val = tmp.substring(ind+1);
          
           if( 'token' === key ){
                flag= true;
           }
       }
       return flag
}

/**
 * 获取token
 */
exports.getToken=function(cookieName){
       // 先获取所有的cookie
       var str = document.cookie;
       // 拆分成数组
       var arr = str.split("; ");
       // 循环判断
       for( var i=0,len=arr.length; i<len; i++ ){
           var tmp = arr[i];
           var ind = tmp.indexOf("=");
           var key = tmp.substring(0, ind);
           var val = tmp.substring(ind+1);
          
           if( cookieName === key ){
                return val;
           }
       }
}
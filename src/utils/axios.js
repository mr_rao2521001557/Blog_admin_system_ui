import axios from "axios";
import NProgress from 'nprogress'
axios.defaults.baseURL = '/api';
import {  Message} from "element-ui"
import dispatchStore from "../const/dispatchStore"
axios.defaults.timeout = 30000;
// 返回其他状态吗
// axios.defaults.validateStatus = function (status) {
//   return status >= 200 && status <= 500; // 默认的
// };
// // 跨域请求，允许保存cookie
// axios.defaults.withCredentials = true;
// NProgress.configure({
//   mininum:0.1,
// trickle: false,
// trickleSpeed: 200,
// showSpinner: false,
// })
// HTTPrequest拦截
axios.interceptors.request.use(
  config => {
    NProgress.start()
      console.log('请求参数option',config)
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

// HTTPresponse拦截
axios.interceptors.response.use(
  res => {
    NProgress.done();   
    return res
  },
  error => {
    NProgress.done();
    return Promise.reject(new Error(error));
  }
);
function send(url,params,extendAxiosOption){
  let options=Object.assign({
    responseType:'json',//默认返回类型
  },extendAxiosOption);
  options.headers=Object.assign({
    // Authorization:"",//访问权限
  })
  return new Promise((resolve,reject)=>{
    axios.request(options).then(res=>{
      console.log("响应结果",res);
      if(res.status===200){
        // Message({
        //   message: res.data.msg,
        //   type: 'success'
        // });
       if(res.data.code===0){
        Message.error(res.data.msg);
       }else if(res.data.code===401){
        window.$router.push({name:"登录"})
        window.$store.dispatch(dispatchStore.LOGIN_STATE,res.data.code)
        Message({
          type:'warning',
          message:res.data.msg
        })
       }else if(res.data.code===200){
        Message({
          type:'success',
          message:res.data.msg
        })
       }
        resolve(res.data)
      }else{
        Message.error('操作失败，请稍后再试');
        reject(res)
      }
    }).catch(error=>{
      Message.error('操作失败，请稍后再试');
      reject(error)
    })
  })
  
}
function get(url,params,options={}){
  const option=
  Object.assign(
    {
      url:url,
      method:"get",
      params:params,
      headers:{
        "Content-Type": "application/json; charset=UTF-8;"
      }
    },
    options
  )
 
  return send(url,params,option)
}
function post(url,params,options={}){
  let option=Object.assign({
    url:url,
    method:"post",
    data:params,
    headers:{
      "Content-Type":"application/json; charset=UTF-8;"
    }
  },options)
  return send(url,params,option)
}

function del(url,params,options={}){
  let option=Object.assign({
    url:url,
    method:"delete",
    data:JSON.stringify(params),
    headers:{
      "Content-Type":"application/json; charset=UTF-8;"
    }
  },options)
  return send(url,params,option)
}
function put(url,params,options={}){
  let option=Object.assign({
    url:url,
    method:"put",
    data:JSON.stringify(params),
    headers:{
      "Content-Type":"application/json; charset=UTF-8;"
    }
  },options)
  return send(url,params,option)
}
export default {get,post,put,del};


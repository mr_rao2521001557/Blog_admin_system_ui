import Vue from 'vue'
import Router from 'vue-router'
import Login from '../views/login/Login'
import Layout from "../components/Layout.vue"
Vue.use(Router)
let asyncRouter= [
  {
    path:"/",
    redirect:"/wel"
  },
  {
    path:"/wel",
    redirect:"/wel/index",
    component:Layout,
    name:"布局",
    mate:{
      title:"permission",
      roles:["admin","editor"]
    },
    children:[
        {
        path:"/wel/home",
        name:"首页",
        component:()=>import('../views/home/Home'),
        mate:{
          title:"page",
          roles:['editor']
        }
      },
    ]
  },
  {
    path: '/login',
    name: '登录',
    component: Login,
    mate:{
      title:"登录",
      roles:['admin',]
    }
  },

  {
    path: '*',
    redirect:"/login",
  }
];
let scrollBehavior=function (to,from,savedPosition){
  if(savedPosition){
    return savedPosition
  }else{
    return {x:0,y:0}
  }
}
export default new Router({
  routes:asyncRouter,
  mode:"hash",
  scrollBehavior //页面位置滚动控制
})

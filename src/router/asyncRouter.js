// asyncRouter.js
// 判断当前角色是否有访问权限
function hasPermission(roles,route){
    if(route.meta&&route.meta.roles){
        //some：遍历，数组中当数组中有一个满足返回true，都不满足返回false
        //includes:数组是否包含某个元素，包含返回true，不包含返回false
        return roles.some(role=>route.meta.roles.includes(role))
    }else{
        return true
    }
}

//递归过滤异步路由表，筛选角色权限路由
export function filterAsyncRouter(routes,roles){
    const res=[];
    routes.forEach(route=>{
        const tem={...route}
        if(hasPermission(roles,tmp)){
            if(tmp.children){
                tmp.children=filterAsyncRouter(tmp.children,roles)
            }
            res.push(tmp)
        }
    })
}
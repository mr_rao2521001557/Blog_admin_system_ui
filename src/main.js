// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from './utils/axios'
import ElementUI from 'element-ui';
import Layout from './components/Layout'
import store from "./store/index"
import "./permission"
import 'element-ui/lib/theme-chalk/index.css';
import './style/common.scss'
import "./style/common.css"
import SIdentify from './components/Identify'
window.$store=store;
window.$router=router;
console.log('window',window)
Vue.component('s-identify',SIdentify);
Vue.component('s-layout',Layout)
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$axios=axios;
/* eslint-disable no-new */
var vm=new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
window.$vm=vm

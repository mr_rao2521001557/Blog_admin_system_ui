const moduleUser={
    state:{
        test:'moduleUser',
        loginState:0  //0未登录  200登录合法 401 登录不合法
     },
    actions:{
        LOGIN_STATE({commit},content){
            commit("LOGIN_STATE",content)
        }
    },
    mutations:{
        LOGIN_STATE(state,content){
            state.loginState=content
        }
    }
}
export default moduleUser